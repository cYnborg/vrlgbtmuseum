using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Globalization;

public class gcode_parser : MonoBehaviour
{
    public bool parseGcode;
    public List<Vector3> xyzlist;
    public List<int> commandList;
    public Vector3[] xyzArray;
    public int[] commandArray;
    public StreamReader reader;
    public string FilePath;
    public float lineThickness;
    private bool startSet = false;
    private int layerNum = 0;
    private Vector3 XYZ;
    private string[] parts;
    private bool xSet = false;
    private bool ySet = false;
    private bool zSet = false;
    private bool doneReading = false;
    // Start is called before the first frame update
    void Start()
    {
        reader = new StreamReader(FilePath);
    }

    // Update is called once per frame
    void Update()
    {
        if (parseGcode)
        {
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();

                if (startSet)
                {
                    if (line.StartsWith(";LAYER:"))
                    {
                        layerNum = int.Parse(line.Substring(7));
                        Debug.Log(layerNum);
                    }
                    else if ((line.StartsWith("G1") || line.StartsWith("G0")))
                    {
                        string[] parts = line.Split(' ');
                        foreach (string part in parts)
                        {
                            if (part.StartsWith("X"))
                            {
                                xSet = true;
                                XYZ.x = float.Parse(part.Substring(1), CultureInfo.InvariantCulture.NumberFormat);
                            }
                            else if (part.StartsWith("Y"))
                            {
                                ySet = true;
                                XYZ.y = float.Parse(part.Substring(1), CultureInfo.InvariantCulture.NumberFormat);
                            }
                            else if (part.StartsWith("Z"))
                            {
                                Debug.Log(part.Substring(1));
                                //zSet = true;
                                XYZ.z = float.Parse(part.Substring(1), CultureInfo.InvariantCulture.NumberFormat);
                            }
                        }
                        if (xSet && ySet)
                        {
                            xyzlist.Add(XYZ);
                            if (line.StartsWith("G1"))
                            {
                                commandList.Add(1);
                            }
                            else
                            {
                                commandList.Add(0);
                            }
                            xSet = false;
                            ySet = false;
                            //zSet = false;
                        }
                    }
                    /*
                    else if (line.StartsWith("G1"))
                    {
                        parts = line.Split(' ');
                        foreach (string part in parts)
                        {
                            if (part.StartsWith("X"))
                            {
                                xSet = true;
                                XYZ.x = float.Parse(part.Substring(1), CultureInfo.InvariantCulture.NumberFormat);
                            }
                            else if (part.StartsWith("Y"))
                            {
                                ySet = true;
                                XYZ.y = float.Parse(part.Substring(1), CultureInfo.InvariantCulture.NumberFormat);
                            }
                            else if (part.StartsWith("Z"))
                            {
                                Debug.Log(part.Substring(1));
                                zSet = true;
                                XYZ.z = float.Parse(part.Substring(1), CultureInfo.InvariantCulture.NumberFormat);

                            }
                        }
                        if (xSet && ySet && zSet)
                        {
                            xyzlist.Add(XYZ);
                            commandList.Add(1);
                            xSet = false;
                            ySet = false;
                            zSet = false;
                        }
                    }
                    */
                }
                if (line.StartsWith(";MESH:"))
                {
                    startSet = true;
                }

            }
            if (reader.EndOfStream)
            {
                doneReading = true;
            }
        }
        if (doneReading)
        {
            xyzArray = xyzlist.ToArray();
            commandArray = commandList.ToArray();
        }
    }
}