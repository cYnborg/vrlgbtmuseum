using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(ActionBasedController))]
public class HandGrabController : MonoBehaviour {
    private ActionBasedController _controller;
    public Rigidbody HandRigidbody;

    [SerializeField] private Transform _grabStart;

    [SerializeField] private float _reachDistance = 0.2f;

    [SerializeField] private LayerMask grabbableLayer;

    private bool _isGrabbing;

    private GameObject _heldObject;

    private Transform _grabPoint;

    private FixedJoint _joint1;

    private FixedJoint _joint2;

    

    void Start() {
        _controller = GetComponent<ActionBasedController>();

        _controller.activateAction.action.started += Grab;
        _controller.activateAction.action.canceled += Release;
    }

    private void Grab(InputAction.CallbackContext obj) {
        Debug.Log("Grab");

        // if we are in the process of grabbing or already holding somenthing do nothing
        if (_isGrabbing || _heldObject) {
            return;
        }

        // check for objects which have the specified layer (Grabbable eg.) within rech position 
        Collider[] grabbableColliders = Physics.OverlapSphere(_grabStart.position, _reachDistance, grabbableLayer);
        if (grabbableColliders.Length < 1) {
            return;
        }

        var grabbableCollider = grabbableColliders[0];
        var objectToGrab = grabbableCollider.transform.gameObject;
        var objectBody = objectToGrab.GetComponent<Rigidbody>();
        if (objectBody != null) {
            _heldObject = objectBody.gameObject;
        }
        else {
            // fallback if rigidbody is in parent because of child colliders
            objectBody = objectToGrab.GetComponentInParent<Rigidbody>();
            if (objectBody != null) {
                _heldObject = objectBody.gameObject;
            }
            else {
                return;
            }
        }

        //StartCoroutine(GrabObject(grabbableCollider, objectBody));
        GrabObject(grabbableCollider, objectBody);

    }

    private void GrabObject(Collider collider, Rigidbody targetBody) {

        _isGrabbing = true;
        
        // Create a grab point
        _grabPoint = new GameObject().transform;
        _grabPoint.position = collider.ClosestPoint((_grabStart.position));
        _grabPoint.parent = _grabStart;


        targetBody.collisionDetectionMode = CollisionDetectionMode.Continuous;
        targetBody.interpolation = RigidbodyInterpolation.Interpolate;

        // Attach joints
        
        _joint1 = _grabPoint.gameObject.AddComponent<FixedJoint>();
        _joint1.connectedBody = targetBody;
        _joint1.breakForce = 50000f;
        _joint1.breakTorque = 50000f;
        _joint1.connectedMassScale = 1;
        _joint1.massScale = 1f;
        _joint1.enableCollision = false;
        _joint1.enablePreprocessing = true;

        _joint2 = _heldObject.AddComponent<FixedJoint>();
        _joint2.connectedBody = HandRigidbody;
        _joint2.breakForce = 50000f;
        _joint2.breakTorque = 50000f;
        _joint2.connectedMassScale = 1;
        _joint2.massScale = 1f;
        _joint2.enableCollision = false;
        _joint2.enablePreprocessing = true;
        
        
        
        
        
        var putExhibitBack = _heldObject.GetComponent<PutExhibitBack>();
        if (putExhibitBack) {
            putExhibitBack.Grab();
        }
        
    }


    private void Release(InputAction.CallbackContext obj) {
        Debug.Log("Release");

        if (_joint1 != null) {
            Destroy(_joint1);
        }
        if (_joint2 != null) {
            Destroy(_joint2);
        }
        if (_grabPoint != null) {
            Destroy(_grabPoint.gameObject);
        }

        if (_heldObject != null) {
            var targetBody = _heldObject.GetComponent<Rigidbody>();


            var putExhibitBack = _heldObject.GetComponent<PutExhibitBack>();
            if (putExhibitBack) {
                putExhibitBack.Release();
            }
            
            _heldObject = null;
        }

        _isGrabbing = false;
        


    }


    void Update() {
    }
}