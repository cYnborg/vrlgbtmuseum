using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


[RequireComponent( typeof(ActionBasedController))]
public class HandAnimController : MonoBehaviour {
    private ActionBasedController controller;
    public HandAnimator HandAnimator;


    void Start() {
        controller = GetComponent<ActionBasedController>();

    }

    void Update() {
        float selectActionValue = controller.selectAction.action.ReadValue<float>();
        float activateActionValue = controller.activateAction.action.ReadValue<float>();
        
        HandAnimator.SetPinky(selectActionValue);
        HandAnimator.SetRing(selectActionValue);
        HandAnimator.SetMiddle(selectActionValue);

        HandAnimator.SetThumb(activateActionValue);
        HandAnimator.SetIndex(activateActionValue);
    }
}
