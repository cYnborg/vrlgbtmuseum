using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PutExhibitBack : MonoBehaviour {
    private float SecondsBeforePutBack = 4f;
    
    private Vector3 _startPosition;
    private Quaternion _startRotation;

    private int _handsGrabbing = 0;

    private float _timeNotInPlace = 0f;

    private float _waitAfterStart = 2f;

    private bool _ready = false;
    
    private Rigidbody _rigidbody;
    
    void Start() {
        _rigidbody = GetComponent<Rigidbody>();

        // static mode
        _rigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;
        _rigidbody.interpolation = RigidbodyInterpolation.None;
        
    }

    void PutBack() {
        transform.position = new Vector3(_startPosition.x,_startPosition.y,_startPosition.z);
        transform.rotation = new Quaternion(_startRotation.x, _startRotation.y,_startRotation.z,_startRotation.w);

        // static mode
        _rigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;
        _rigidbody.interpolation = RigidbodyInterpolation.None;

        _timeNotInPlace = 0f;
    }
    
    void Update() {
        if (!_ready) {
            _waitAfterStart = _waitAfterStart - Time.deltaTime;
            if (_waitAfterStart < 0f) {
                _startPosition =  new Vector3(transform.position.x,transform.position.y,transform.position.z);       
                _startRotation =  new Quaternion(transform.rotation.x, transform.rotation.y,transform.rotation.z,transform.rotation.w);
                _ready = true;
            }
        }
        
        if (_ready &&  !IsGrabbed()  && !InPlace()) {
            
            // dynamic mode
            _rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
            _rigidbody.interpolation = RigidbodyInterpolation.Interpolate;

            
            
            _timeNotInPlace += Time.deltaTime;
            if (_timeNotInPlace > SecondsBeforePutBack) {
                PutBack();
            }                
        }
        else {
            _timeNotInPlace = 0f;
        }       
    }
    
    public void Grab() {
        _handsGrabbing++;
    }
    
    public void Release() {
        _handsGrabbing--;
    }

    bool IsGrabbed() {
        return _handsGrabbing > 0;
    }

    bool InPlace() {
        if (!(RotationApprox(transform.rotation, _startRotation))) {
            return false;
        }
        if (!(PositionApprox(transform.position, _startPosition))) {
            return false;
        }

        return true;
    }
    
    bool RotationApprox(Quaternion r1, Quaternion r2)
    {
        float abs = Mathf. Abs(Quaternion. Dot(r1, r2));
        if (abs >= 0.9999999f) {
            return true;
        }
        else {
            return false;
        }
    }

    
    bool PositionApprox(Vector3 v1, Vector3 v2) {
        return v1 == v2;
    }


}
